import datetime
import json
import logging
import requests
import time

from django.core.management.base import BaseCommand
from ingress.ingress.utils import within_range

from .utils import HEADERS, cookie_need_update, PAYLOAD_V



def get_payload(code):
    return {
        'passcode': code,
        'v': PAYLOAD_V,
    }

class Command(BaseCommand):
    args = ''
    help = 'Fetch portals base on map tiles'

    def handle(self, *args, **options):
        myfile = open("passcodes.txt")
        for line in myfile:
            line = line.replace('\n', '')
            print ("Send passcode: "+line)

            payload = get_payload(line)
            try:
                r = requests.post(
                        "https://www.ingress.com/r/redeemReward",
                    data=json.dumps(payload),
                    headers=HEADERS
                )
            except:
                logging.exception('Error:')
                return

            try:
                result = json.loads(r.text)
            except:
                logging.exception('')
                logging.error(r.text)
                return

            if ('rewards' in result):
                print("Rewards: "+str(result['rewards']))
            elif ('error' in result):
                print("Error: "+result['error'])
                if (result['error'] == 'XM object capacity reached.' or result['error'] == 'Passcode circuitry too hot. Wait for cool down to enter another passcode.'):
                    break

            print(result)
            time.sleep(1.0)
