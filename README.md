Ingress Intel for Your City
===========================


This Project Has Stoped
-----------------------

Due to violating TOS of Ingress and Hello from NianticOps.


Rules You Should Follow When Using this Project
-----------------------------------------------

- Only collect one city on one server except the Ingress actions in collecting area are very few.
- Request once or less per minute (once every 5 or 10 minutes may satisfy small citys)
- Do not show old actions info for any agents (like data 90 days ago)


Requirements
------------

- Unix/Linux (Windows is strongly not recommended)
- Python 3.4+
- Django 1.7+
- requests


Install on Debian/Ubuntu
----------------------------

sudo apt-get install python3 python3-pip

sudo pip3 install django requests

Usage
-----

1) Open a file called `settings_local.py` under the directory `ingress`.

2) Login your Ingress account (may be banned :-( ) in Mozilla Firefox or Google Chrome browser. Use Chrome's [Inspect Element](https://developer.chrome.com/devtools) to find out the [Request Headers](https://developer.chrome.com/devtools/docs/network#http-headers) of request `POST /r/getPlexts`, which will needed in step 3 or Inspector in Firefox.

3) Edit the following values in `settings_local.py`:

```
INGRESS_INTEL_COOKIE = ""
INGRESS_INTEL_CSRF_TOKEN = ""
INGRESS_INTEL_PAYLOAD_V = ""
```

and update the region range（You can find the Lat/Lng with [Google Map](https://www.google.com/maps/preview) ):

```
MIN_LAT = 41636215
MAX_LAT = 43761852
MIN_LNG = 141825375
MAX_LNG = 146483578
```

4) Then we're ready to go (every time you run it, please **make sure** remove `~/.ingress/need_update.txt` if it exists)：

`python3 manage.py test_collect`

If you see some JSON-like outputs, then we are succeed. Otherwise, Please do step 2 and step 3 again.

5) Database migrations

```
python3 manage.py migrate
```

6) Collect for real

```
python3 manage.py collect
```

7) Run Server and see.

```
python3 manage.py runserver 8080
```

Open browser to see `http://127.0.0.1:8080/`

8) Use `python3 manage.py help` to see other ingress commands.


Enter passcodes
---------------

1) Create a file called `passcodes.txt` in the this directory.

2) Run sending and see. Program will be stopped if inventory is full.

```
python3 manage.py passcode
```